const mongoose = require('mongoose');
//const Schema = mongoose.Schema;  E' lo stesso
const { Schema } = mongoose;

const userSchema = new Schema({
  googleId: String
});

mongoose.model('users', userSchema);
