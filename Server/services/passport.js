const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const mongoose = require('mongoose');

// Importa il contenuto del file keys.js
const keys = require('../config/keys')

const User = mongoose.model('users');

// user è l'oggetto che ho passato al "done" del passport.use
// il serializeUser crea una stringa che andrà scritta nel cookie
passport.serializeUser((user, done) => {
  done(null, user.id); // Questo id è quello di MongoDB
});

// il deserializeUser legge la stringa di ritorno dal cookie
passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => {
      done(null, user);
    });
});

// Configurazione del Google Strategy
// console.developers.google.com
passport.use(
  // proxy: true // Senza google farebbe la redirect al sito http:// mentre il verso sito è https://
  new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: '/auth/google/callback',
    proxy: true
  },
  (accessToken, refreshToken, profile, done) => {
    // Faccio una query a MongoDB
    User.findOne({ googleId: profile.id })
      // La query è asincrona e torna un "promise"
      .then((existingUser) => {
        if (existingUser){
          // c'è già un record e lo passo a passport che lo userà nella serializeUser
          done(null, existingUser);
        }else{
          // Non c'è il record ne creo uno nuovo
          new User({ googleId: profile.id }).save()
            // save è asincrono e passo la user a passport che la userà nella serializeUser
            .then( user => done(null, user) );
        }
      })
    //console.log('access token', accessToken);
    //console.log('refresh token', refreshToken);
    //console.log('profile', profile);
  //  () => {
  //  console.log('ecchime2');
  })
);
// Questo mandava in errore "http://localhost:5000/auth/auth/google/callback"
//  callbackURL: 'auth/google/callback'
// Questo mandava in crash passport: TokenError: Bad Request
//  callbackURL: 'google/callback'
// per un solo parametro accessToken => {} è lo stesso di (accessToken) => {}
