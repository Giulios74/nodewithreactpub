// keys.js
if (process.env.NODE_ENV === 'production'){
  // Siamo in produzione
  module.exports = require('./prod');
}else{
  // Siamo in sviluppo
  module.exports = require('./dev');
}
