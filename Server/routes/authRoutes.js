const passport = require('passport');

module.exports = app => {
//module.exports = (app) => {
  app.get('/auth/google',
    passport.authenticate('google', {
      scope: ['profile', 'email']
    })
  );

  app.get('/auth/google/callback',
    passport.authenticate('google')
  );

  app.get('/api/logout', (req, res) => {
    req.logout();
    res.send(req.user);
  });

  app.get('/api/current_user', (req, res) => {
    //res.send(req.session);
    res.send(req.user);
  });

  // Utile per certificare il dominio a google
  app.get('/google5cabb616b65445d4.html', (req, res) => {
    //res.send(req.session);
    res.send('google-site-verification: google5cabb616b65445d4.html');
  });

};
// Se uso /api/current _ user va in errore?!?!?
// Basta riavviare il servizio e riprovare

/*
app.get('/auth/google',
  passport.authenticate('google', {
    scope: ['profile', 'email']
  })
);
// Lo scope sono le funzionalità alle quali si avrà accesso

app.get('/auth/google/callback',
  passport.authenticate('google')
  //(request,response) => {
  //  response.send('eccomi');
  //}
  //passport.authenticate('google')
);
*/
