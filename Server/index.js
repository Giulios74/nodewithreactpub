// Express semplifica la comunicazione HTTP
const express = require('express');
// Operazioni su DB MongoDB
const mongoose = require('mongoose');
// gestisce i cookie della sessione
const cookieSession = require('cookie-session');
// gestisce l'autenticazione tramite google
const passport = require('passport');
// Config
const keys = require('./config/keys');
// Modello User
require("./models/User");
// Importa un modilo per gestire oauth20 di google
require("./services/passport");

mongoose.connect(keys.mongoURI);

// Gestione veloce dell'http
const app = express();

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
app.use(passport.initialize());
app.use(passport.session());

// Gestisco le routes
require("./routes/authRoutes")(app);

// app in ascolto sulle porta scelta
const PORT = process.env.PORT || 5000;
app.listen(PORT);
